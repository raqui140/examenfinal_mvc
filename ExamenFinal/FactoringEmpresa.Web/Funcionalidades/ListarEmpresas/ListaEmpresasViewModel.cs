﻿namespace FactoringEmpresa.Web.Funcionalidades.ListarEmpresas
{
    public class ListaEmpresasViewModel
    {
        public string Ruc { get; set; }
        public string RazonSocial { get; set; }
        public string Rubro { get; set; }
    }
}