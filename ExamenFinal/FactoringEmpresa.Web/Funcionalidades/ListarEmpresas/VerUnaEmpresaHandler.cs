﻿using FactoringEmpresa.Repositorio;
using FactoringEmpresa.Repositorio.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FactoringEmpresa.Web.Funcionalidades.ListarEmpresas
{
    public class VerUnaEmpresaHandler : IDisposable
    {
        private readonly FactoringEmpresaRepositorio facturaempresaRepositorio;

        public VerUnaEmpresaHandler()
        {
            facturaempresaRepositorio = new FactoringEmpresaRepositorio(new FactoringEmpresaContexto());
        }

        public VerUnaEmpresaViewModel Execute(string id)
        {
            var empresa = facturaempresaRepositorio.Empresa.TraerUno(x => x.Ruc == id);
            return new VerUnaEmpresaViewModel()
            {
                Ruc = empresa.Ruc,
                RazonSocial = empresa.RazonSocial,
                Direccion = empresa.Direccion,
                Rubro = empresa.Rubro,
                Departamento = empresa.Departamento,
                Provincia = empresa.Provincia,
                Distrito = empresa.Distrito
            };
        }

        public void Dispose()
        {
            facturaempresaRepositorio.Dispose();
        }
    }
}