﻿using FactoringEmpresa.Repositorio;
using FactoringEmpresa.Repositorio.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace FactoringEmpresa.Web.Funcionalidades.ListarEmpresas
{
    public class ListarEmpresasHandler : IDisposable
    {
        private readonly FactoringEmpresaRepositorio facturaRepositorio;

        public ListarEmpresasHandler()
        {
            facturaRepositorio = new FactoringEmpresaRepositorio(new FactoringEmpresaContexto());
        }
        public void Dispose()
        {
            facturaRepositorio.Dispose();
        }

        public IEnumerable<ListaEmpresasViewModel> Ejecutar(string usuario)
        {
            var consulta = facturaRepositorio.Empresa.TraeVarios(x => x.IdUsuario == usuario );

            return consulta.Select(e =>
                        new ListaEmpresasViewModel()
                        {
                            Ruc = e.Ruc,
                            RazonSocial = e.RazonSocial,
                            Rubro = e.Rubro
                        }
                    ).ToList();
            
        }
    }
}