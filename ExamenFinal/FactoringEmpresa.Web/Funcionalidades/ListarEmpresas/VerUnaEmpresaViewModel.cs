﻿namespace FactoringEmpresa.Web.Funcionalidades.ListarEmpresas
{
    public class VerUnaEmpresaViewModel
    {
        public string Ruc { get; set; }
        public string RazonSocial { get; set; }
        public string Direccion { get; set; }
        public string Rubro { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }

    }
}