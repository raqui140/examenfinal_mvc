﻿using FactoringEmpresa.Modelo;
using FactoringEmpresa.Repositorio;
using FactoringEmpresa.Repositorio.Impl;
using System;

namespace FactoringEmpresa.Web.Funcionalidades.RegistrarEmpresa
{
    public class RegistrarEmpresaHandler : IDisposable
    {
        private readonly FactoringEmpresaRepositorio facturaempresaRepositorio;

        public RegistrarEmpresaHandler()
        {
            facturaempresaRepositorio = new FactoringEmpresaRepositorio(new FactoringEmpresaContexto());
        }

        public void Ejecutar(RegistrarEmpresaViewModel modelo, string idUsuario)
        {
            Empresa empresa = CreaEmpresa(modelo, idUsuario);
            facturaempresaRepositorio.Empresa.Agregar(empresa);
            facturaempresaRepositorio.Commit();
        }

        private Empresa CreaEmpresa(RegistrarEmpresaViewModel modelo, string idUsuario)
        {
            
            return new Empresa()
            {
                Ruc = modelo.Ruc,
                RazonSocial = modelo.RazonSocial,
                Direccion = modelo.Direccion,
                Rubro = modelo.Rubro,
                IdUsuario = idUsuario,
                Departamento = modelo.Departamento,
                Provincia = modelo.Provincia,
                Distrito = modelo.Distrito
            };
        }

        public void Dispose()
        {
            facturaempresaRepositorio.Dispose();
        }
    }
}