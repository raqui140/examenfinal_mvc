﻿using FluentValidation;
using System;

namespace FactoringEmpresa.Web.Funcionalidades.RegistrarEmpresa
{
    public class RegistrarEmpresaViewModelValidator: AbstractValidator<RegistrarEmpresaViewModel>
    {
        public RegistrarEmpresaViewModelValidator()
        {
            RuleFor(modelo => modelo.Ruc)
                .NotEmpty()
                .Length(11)
                .WithMessage("El Ruc debe tener 11 caracteres.");

            RuleFor(modelo => modelo.Ruc.Substring(0, 1))
                .Equal("2")
                .WithMessage("El Ruc ingresado debe ser de Empresa Jurídica.");

            RuleFor(modelo => modelo.Ruc)
                .Must(ValidaRuc11Dig)
                .WithMessage("Ruc Inválido");

            RuleFor(modelo => modelo.RazonSocial)
                .NotEmpty()
                .Length(2,150)
                .WithMessage("La Razon Social debe tener una longitud entre 2 a 150 caracteres.");
        }

        private bool ValidaRuc11Dig(string ruc)
        {
            ruc = ruc.Trim();
            if (!string.IsNullOrEmpty(ruc))
            {   
                int addition = 0;
                int[] hash = { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };
                int identificationDocumentLength = ruc.Length;

                string identificationComponent = ruc.Substring(0, identificationDocumentLength - 1);

                int identificationComponentLength = identificationComponent.Length;

                int diff = hash.Length - identificationComponentLength;

                for (int i = identificationComponentLength - 1; i >= 0; i--)
                {
                    addition += (identificationComponent[i] - '0') * hash[i + diff];
                }

                addition = 11 - (addition % 11);

                if (addition == 11)
                {
                    addition = 0;
                }

                char last = char.ToUpperInvariant(ruc[identificationDocumentLength - 1]);

                if (identificationDocumentLength == 11)
                {
                    // The identification document corresponds to a RUC.
                    return addition.Equals(last - '0');
                }
                else if (char.IsDigit(last))
                {
                    // The identification document corresponds to a DNI with a number as verification digit.
                    char[] hashNumbers = { '6', '7', '8', '9', '0', '1', '1', '2', '3', '4', '5' };
                    return last.Equals(hashNumbers[addition]);
                }
                else if (char.IsLetter(last))
                {
                    // The identification document corresponds to a DNI with a letter as verification digit.
                    char[] hashLetters = { 'K', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J' };
                    return last.Equals(hashLetters[addition]);
                }
            }

            return false;
        }
    }
}