﻿using FluentValidation.Attributes;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FactoringEmpresa.Web.Funcionalidades.RegistrarEmpresa
{
    [Validator(typeof(RegistrarEmpresaViewModelValidator))]
    public class RegistrarEmpresaViewModel
    {
        public string Ruc { get; set; }

        public string RazonSocial { get; set; }
        public string Direccion { get; set; }
        public string Rubro { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string Usuario { get; set; }
        public SelectList Departamentos { get; set; }

        public SelectList Provincias { get; set; }

        public SelectList Distritos { get; set; }

        public RegistrarEmpresaViewModel()
        {
            var listaVacia = new List<string>() { "Seleccione..." };
            Departamentos = new SelectList(listaVacia);
            Provincias = new SelectList(listaVacia);
            Distritos = new SelectList(listaVacia);
        }
    }
}