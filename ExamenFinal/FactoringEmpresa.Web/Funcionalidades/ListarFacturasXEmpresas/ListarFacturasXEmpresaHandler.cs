﻿using FactoringEmpresa.Repositorio;
using FactoringEmpresa.Repositorio.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FactoringEmpresa.Web.Funcionalidades.ListarFacturasXEmpresas
{
    public class ListarFacturasXEmpresaHandler : IDisposable
    {
        private readonly FactoringEmpresaRepositorio facturaEmpresaRepositorio;

        public ListarFacturasXEmpresaHandler()
        {
            facturaEmpresaRepositorio = new FactoringEmpresaRepositorio(new FactoringEmpresaContexto());
        }

        public IEnumerable<ListarFacturasXEmpresaViewModel> Ejecutar(string ruc)
        {
            var consulta = facturaEmpresaRepositorio.Factura.TraeVarios(x => x.RucCliente == ruc);

            return consulta.Select(e =>
                        new ListarFacturasXEmpresaViewModel()
                        {
                            Serie = e.Serie,
                            Numero = e.Numero,
                            FechaEmision = e.FechaEmision,
                            FechaVencimiento = e.FechaVencimiento,
                            FechaCobro = e.FechaCobro,
                            RucEmpresa = e.RucCliente,
                            RazonSocial = e.RazonSocialCliente,
                            TotalFactura = e.TotalFactura,
                            TotalImpuesto = e.TotalIGV
                        }
                    ).ToList();

        }
        public void Dispose()
        {
            facturaEmpresaRepositorio.Dispose();
        }
    }
}