﻿using System;
    
namespace FactoringEmpresa.Web.Funcionalidades.ListarFacturasXEmpresas
{
    public class ListarFacturasXEmpresaViewModel
    {
        public string Serie { get; set; }
        public string Numero { get; set; }
        public DateTime FechaEmision { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public DateTime FechaCobro { get; set; }
        public string RucEmpresa { get; set; }
        public string RazonSocial { get; set; }
        public decimal TotalFactura { get; set; }
        public decimal TotalImpuesto { get; set; }
    }
}