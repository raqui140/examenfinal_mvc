﻿using FactoringEmpresa.Modelo;
using FactoringEmpresa.Repositorio;
using FactoringEmpresa.Repositorio.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FactoringEmpresa.Web.Funcionalidades.RegistrarFacturaXEmpresa
{
    public class RegistrarFacturaXEmpresaHandler : IDisposable
    {
        private readonly FactoringEmpresaRepositorio facturaempresaRepositorio;

        public RegistrarFacturaXEmpresaHandler()
        {
            facturaempresaRepositorio = new FactoringEmpresaRepositorio(new FactoringEmpresaContexto());
        }

        public void Ejecutar(RegistrarFacturaXEmpresaViewModel modelo)
        {
            Factura factura = CreaFactura(modelo);
            facturaempresaRepositorio.Factura.Agregar(factura);
            facturaempresaRepositorio.Commit();
        }

        private Factura CreaFactura(RegistrarFacturaXEmpresaViewModel modelo)
        {

            return new Factura()
            {
                Serie = modelo.Serie,
                Numero = modelo.Numero,
                FechaEmision = modelo.FechaEmision,
                FechaVencimiento = modelo.FechaVencimiento,
                FechaCobro = modelo.FechaCobro,
                RucCliente = modelo.RucEmpresa,
                RazonSocialCliente = modelo.RazonSocial,
                TotalFactura = modelo.TotalFactura,
                TotalIGV = modelo.TotalImpuesto
            };
        }
        public void Dispose()
        {
            facturaempresaRepositorio.Dispose();
        }
    }
}