﻿using FactoringEmpresa.Web.Funcionalidades.ListarFacturasXEmpresas;
using FactoringEmpresa.Web.Funcionalidades.RegistrarFacturaXEmpresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FactoringEmpresa.Web.Controllers
{
    [Authorize]
    public class FacturaController : Controller
    {
        // GET: Factura
        public ActionResult Lista(string id)
        {
            using (var listarFacturas = new ListarFacturasXEmpresaHandler())
            {
                return View(listarFacturas.Ejecutar(id));
            }
        }

        [HttpGet]
        public PartialViewResult Registrar(string id, string empresa)
        {
            return PartialView("_Registrar", new RegistrarFacturaXEmpresaViewModel()
            {
                RucEmpresa = id,
                RazonSocial = empresa
            });
        }

        [HttpPost]
        public ActionResult Registrar(RegistrarFacturaXEmpresaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);

            using (var registrarFactura = new RegistrarFacturaXEmpresaHandler())
            {
                try
                {
                    registrarFactura.Ejecutar(modelo);
                    return RedirectToAction("Lista", "Empresa");

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }

            }
        }
    }
}