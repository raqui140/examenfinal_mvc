﻿using FactoringEmpresa.Web.Funcionalidades.ListarEmpresas;
using FactoringEmpresa.Web.Funcionalidades.RegistrarEmpresa;
using IdentitySample.Models;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace FactoringEmpresa.Web.Controllers
{
    [Authorize]
    public class EmpresaController : Controller
    {
        // GET: Empresa
        public ActionResult Lista()
        {
            using (var listarEmpresas = new ListarEmpresasHandler())
            {
                return View(listarEmpresas.Ejecutar(User.Identity.Name));
            }
        }

        [HttpGet]
        public ActionResult Registrar()
        {
            return View(new RegistrarEmpresaViewModel());
        }

        [HttpPost]
        public ActionResult Registrar(RegistrarEmpresaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);
            
            using (var registrarEmpresa = new RegistrarEmpresaHandler())
            {
                try
                {   
                    registrarEmpresa.Ejecutar(modelo, User.Identity.Name);
                    return RedirectToAction("Lista");

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }

            }
        }

        [HttpGet]
        public PartialViewResult Editar(string id)
        {
            using (var buscarEmpresa = new VerUnaEmpresaHandler())
            {
                return PartialView("_Editar", buscarEmpresa.Execute(id));
            }
        }
        [HttpPost]
        public ActionResult Editar(VerUnaEmpresaViewModel model)
        {
            return RedirectToAction("Lista");
        }

        [HttpGet]
        public PartialViewResult Eliminar(string id)
        {
            using (var buscarEmpresa = new VerUnaEmpresaHandler())
            {
                return PartialView("_Eliminar", buscarEmpresa.Execute(id));
            }
        }

        [HttpPost]
        public ActionResult Eliminar(VerUnaEmpresaViewModel model)
        {
            return RedirectToAction("Lista");
        }

    }
}