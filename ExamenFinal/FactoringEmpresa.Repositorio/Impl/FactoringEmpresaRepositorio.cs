﻿using FactoringEmpresa.Modelo;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoringEmpresa.Repositorio.Impl
{
    public class FactoringEmpresaRepositorio : IDisposable
    {
        private readonly DbContext bd;

        private readonly RepositorioGenerico<Empresa> _empresa;
        private readonly RepositorioGenerico<Factura> _factura;
        private readonly RepositorioGenerico<FacturaImagen> _facturaImagen;

        public FactoringEmpresaRepositorio(DbContext bd)
        {
            this.bd = bd;
            _empresa = new RepositorioGenerico<Empresa>(bd);
            _factura = new RepositorioGenerico<Factura>(bd);
            _facturaImagen = new RepositorioGenerico<FacturaImagen>(bd);
        }

        public RepositorioGenerico<Empresa> Empresa { get { return _empresa; } }
        public RepositorioGenerico<Factura> Factura { get { return _factura; } }
        public RepositorioGenerico<FacturaImagen> FacturaImagen { get { return _facturaImagen; } }

        public void Commit()
        {
            bd.SaveChanges();
        }
        public void Dispose()
        {
            bd.Dispose();
        }
    }
}
