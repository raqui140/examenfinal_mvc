namespace FactoringEmpresa.Repositorio
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Modelo;

    public partial class FactoringEmpresaContexto : DbContext
    {
        public FactoringEmpresaContexto()
            : base("name=FactoringEmpresaContexto")
        {
        }

        public virtual DbSet<Empresa> Empresa { get; set; }
        public virtual DbSet<Factura> Factura { get; set; }
        public virtual DbSet<FacturaImagen> FacturaImagen { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Empresa>()
                .Property(e => e.Ruc)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.RazonSocial)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Departamento)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Provincia)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Distrito)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Rubro)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .HasMany(e => e.Factura)
                .WithRequired(e => e.Empresa)
                .HasForeignKey(e => e.RucCliente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Factura>()
                .Property(e => e.Serie)
                .IsUnicode(false);

            modelBuilder.Entity<Factura>()
                .Property(e => e.Numero)
                .IsUnicode(false);

            modelBuilder.Entity<Factura>()
                .Property(e => e.RucCliente)
                .IsUnicode(false);

            modelBuilder.Entity<Factura>()
                .Property(e => e.RazonSocialCliente)
                .IsUnicode(false);

            modelBuilder.Entity<Factura>()
                .Property(e => e.TotalFactura)
                .HasPrecision(15, 2);

            modelBuilder.Entity<Factura>()
                .Property(e => e.TotalIGV)
                .HasPrecision(15, 2);

            modelBuilder.Entity<Factura>()
                .HasOptional(e => e.FacturaImagen)
                .WithRequired(e => e.Factura);

            modelBuilder.Entity<FacturaImagen>()
                .Property(e => e.Serie)
                .IsUnicode(false);

            modelBuilder.Entity<FacturaImagen>()
                .Property(e => e.Numero)
                .IsUnicode(false);
        }
    }
}
