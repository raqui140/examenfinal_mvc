namespace FactoringEmpresa.Repositorio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicio_Factura : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Empresa",
                c => new
                    {
                        Ruc = c.String(nullable: false, maxLength: 11, unicode: false),
                        RazonSocial = c.String(nullable: false, maxLength: 250, unicode: false),
                        Direccion = c.String(maxLength: 250, unicode: false),
                        Departamento = c.String(maxLength: 50, unicode: false),
                        Provincia = c.String(maxLength: 50, unicode: false),
                        Distrito = c.String(maxLength: 50, unicode: false),
                        Rubro = c.String(maxLength: 150, unicode: false),
                        IdUsuario = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Ruc);
            
            CreateTable(
                "dbo.Factura",
                c => new
                    {
                        Serie = c.String(nullable: false, maxLength: 3, unicode: false),
                        Numero = c.String(nullable: false, maxLength: 5, unicode: false),
                        FechaEmision = c.DateTime(nullable: false, storeType: "date"),
                        FechaVencimiento = c.DateTime(nullable: false, storeType: "date"),
                        FechaCobro = c.DateTime(nullable: false, storeType: "date"),
                        RucCliente = c.String(nullable: false, maxLength: 11, unicode: false),
                        RazonSocialCliente = c.String(nullable: false, maxLength: 250, unicode: false),
                        TotalFactura = c.Decimal(nullable: false, precision: 15, scale: 2),
                        TotalIGV = c.Decimal(nullable: false, precision: 15, scale: 2),
                    })
                .PrimaryKey(t => new { t.Serie, t.Numero })
                .ForeignKey("dbo.Empresa", t => t.RucCliente)
                .Index(t => t.RucCliente);
            
            CreateTable(
                "dbo.FacturaImagen",
                c => new
                    {
                        Serie = c.String(nullable: false, maxLength: 3, unicode: false),
                        Numero = c.String(nullable: false, maxLength: 5, unicode: false),
                        FacturaScan = c.Binary(),
                    })
                .PrimaryKey(t => new { t.Serie, t.Numero })
                .ForeignKey("dbo.Factura", t => new { t.Serie, t.Numero })
                .Index(t => new { t.Serie, t.Numero });
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Factura", "RucCliente", "dbo.Empresa");
            DropForeignKey("dbo.FacturaImagen", new[] { "Serie", "Numero" }, "dbo.Factura");
            DropIndex("dbo.FacturaImagen", new[] { "Serie", "Numero" });
            DropIndex("dbo.Factura", new[] { "RucCliente" });
            DropTable("dbo.FacturaImagen");
            DropTable("dbo.Factura");
            DropTable("dbo.Empresa");
        }
    }
}
