namespace FactoringEmpresa.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Empresa")]
    public partial class Empresa
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Empresa()
        {
            Factura = new HashSet<Factura>();
        }

        [Key]
        [StringLength(11)]
        public string Ruc { get; set; }

        [Required]
        [StringLength(250)]
        public string RazonSocial { get; set; }

        [StringLength(250)]
        public string Direccion { get; set; }

        [StringLength(50)]
        public string Departamento { get; set; }

        [StringLength(50)]
        public string Provincia { get; set; }

        [StringLength(50)]
        public string Distrito { get; set; }

        [StringLength(150)]
        public string Rubro { get; set; }

        [Required]
        [StringLength(128)]
        public string IdUsuario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Factura> Factura { get; set; }
    }
}
