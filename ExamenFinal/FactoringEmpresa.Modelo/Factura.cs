namespace FactoringEmpresa.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Factura")]
    public partial class Factura
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(3)]
        public string Serie { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(5)]
        public string Numero { get; set; }

        [Column(TypeName = "date")]
        public DateTime FechaEmision { get; set; }

        [Column(TypeName = "date")]
        public DateTime FechaVencimiento { get; set; }

        [Column(TypeName = "date")]
        public DateTime FechaCobro { get; set; }

        [Required]
        [StringLength(11)]
        public string RucCliente { get; set; }

        [Required]
        [StringLength(250)]
        public string RazonSocialCliente { get; set; }

        public decimal TotalFactura { get; set; }

        public decimal TotalIGV { get; set; }

        public virtual Empresa Empresa { get; set; }

        public virtual FacturaImagen FacturaImagen { get; set; }
    }
}
