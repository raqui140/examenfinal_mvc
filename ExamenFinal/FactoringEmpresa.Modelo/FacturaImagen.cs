namespace FactoringEmpresa.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FacturaImagen")]
    public partial class FacturaImagen
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(3)]
        public string Serie { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(5)]
        public string Numero { get; set; }

        public byte[] FacturaScan { get; set; }

        public virtual Factura Factura { get; set; }
    }
}
